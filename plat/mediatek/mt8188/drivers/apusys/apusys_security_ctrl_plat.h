/*
 * Copyright (c) 2021, Mediatek Inc. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#ifndef APUSYS_SECURITY_CTRL_PLAT_H
#define APUSYS_SECURITY_CTRL_PLAT_H

#include <platform_def.h>

#define SOC2APU_SET1_0 (APU_SEC_CON + 0xC)
#define SOC2APU_SET1_1 (APU_SEC_CON + 0x10)

#define REG_DOMAIN_NUM   8
#define DOMAIN_REMAP_SEL 6

#endif
