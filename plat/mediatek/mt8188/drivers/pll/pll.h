/*
 * Copyright (c) 2022, MediaTek Inc. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#ifndef PLL_H
#define PLL_H
#include <platform_def.h>

/* APMIXED */
#define AP_PLL_CON0		(APMIXED_BASE + 0x0)
#define AP_PLL_CON1		(APMIXED_BASE + 0x4)
#define AP_PLL_CON2		(APMIXED_BASE + 0x8)
#define AP_PLL_CON3		(APMIXED_BASE + 0xc)
#define APLL1_TUNER_CON0	(APMIXED_BASE + 0x34)
#define APLL2_TUNER_CON0	(APMIXED_BASE + 0x38)
#define APLL3_TUNER_CON0	(APMIXED_BASE + 0x3c)
#define APLL4_TUNER_CON0	(APMIXED_BASE + 0x40)
#define APLL5_TUNER_CON0	(APMIXED_BASE + 0x44)
#define ULPOSC_CTRL_SEL		(APMIXED_BASE + 0x4C)
#define ARMPLL_LL_CON0		(APMIXED_BASE + 0x204)
#define ARMPLL_LL_CON1		(APMIXED_BASE + 0x208)
#define ARMPLL_LL_CON2		(APMIXED_BASE + 0x20c)
#define ARMPLL_LL_CON3		(APMIXED_BASE + 0x210)
#define ARMPLL_BL_CON0		(APMIXED_BASE + 0x214)
#define ARMPLL_BL_CON1		(APMIXED_BASE + 0x218)
#define ARMPLL_BL_CON2		(APMIXED_BASE + 0x21c)
#define ARMPLL_BL_CON3		(APMIXED_BASE + 0x220)
#define CCIPLL_CON0		(APMIXED_BASE + 0x224)
#define CCIPLL_CON1		(APMIXED_BASE + 0x228)
#define CCIPLL_CON2		(APMIXED_BASE + 0x22c)
#define CCIPLL_CON3		(APMIXED_BASE + 0x230)
#define APLL1_CON0		(APMIXED_BASE + 0x304)
#define APLL1_CON1		(APMIXED_BASE + 0x308)
#define APLL1_CON2		(APMIXED_BASE + 0x30c)
#define APLL1_CON3		(APMIXED_BASE + 0x310)
#define APLL1_CON4		(APMIXED_BASE + 0x314)
#define APLL2_CON0		(APMIXED_BASE + 0x318)
#define APLL2_CON1		(APMIXED_BASE + 0x31c)
#define APLL2_CON2		(APMIXED_BASE + 0x320)
#define APLL2_CON3		(APMIXED_BASE + 0x324)
#define APLL2_CON4		(APMIXED_BASE + 0x328)
#define APLL3_CON0		(APMIXED_BASE + 0x32c)
#define APLL3_CON1		(APMIXED_BASE + 0x330)
#define APLL3_CON2		(APMIXED_BASE + 0x334)
#define APLL3_CON3		(APMIXED_BASE + 0x338)
#define APLL3_CON4		(APMIXED_BASE + 0x33c)
#define MFGPLL_CON0		(APMIXED_BASE + 0x340)
#define MFGPLL_CON1		(APMIXED_BASE + 0x344)
#define MFGPLL_CON2		(APMIXED_BASE + 0x348)
#define MFGPLL_CON3		(APMIXED_BASE + 0x34c)
#define APLL4_CON0		(APMIXED_BASE + 0x404)
#define APLL4_CON1		(APMIXED_BASE + 0x408)
#define APLL4_CON2		(APMIXED_BASE + 0x40c)
#define APLL4_CON3		(APMIXED_BASE + 0x410)
#define APLL4_CON4		(APMIXED_BASE + 0x414)
#define APLL5_CON0		(APMIXED_BASE + 0x418)
#define APLL5_CON1		(APMIXED_BASE + 0x41c)
#define APLL5_CON2		(APMIXED_BASE + 0x420)
#define APLL5_CON3		(APMIXED_BASE + 0x424)
#define APLL5_CON4		(APMIXED_BASE + 0x428)
#define ADSPPLL_CON0		(APMIXED_BASE + 0x42c)
#define ADSPPLL_CON1		(APMIXED_BASE + 0x430)
#define ADSPPLL_CON2		(APMIXED_BASE + 0x434)
#define ADSPPLL_CON3		(APMIXED_BASE + 0x438)
#define MPLL_CON0		(APMIXED_BASE + 0x43c)
#define MPLL_CON1		(APMIXED_BASE + 0x440)
#define MPLL_CON2		(APMIXED_BASE + 0x444)
#define MPLL_CON3		(APMIXED_BASE + 0x448)
#define ETHPLL_CON0		(APMIXED_BASE + 0x44c)
#define ETHPLL_CON1		(APMIXED_BASE + 0x450)
#define ETHPLL_CON2		(APMIXED_BASE + 0x454)
#define ETHPLL_CON3		(APMIXED_BASE + 0x458)
#define MAINPLL_CON0		(APMIXED_BASE + 0x45c)
#define MAINPLL_CON1		(APMIXED_BASE + 0x460)
#define MAINPLL_CON2		(APMIXED_BASE + 0x464)
#define MAINPLL_CON3		(APMIXED_BASE + 0x468)
#define UNIVPLL_CON0		(APMIXED_BASE + 0x504)
#define UNIVPLL_CON1		(APMIXED_BASE + 0x508)
#define UNIVPLL_CON2		(APMIXED_BASE + 0x50c)
#define UNIVPLL_CON3		(APMIXED_BASE + 0x510)
#define MSDCPLL_CON0		(APMIXED_BASE + 0x514)
#define MSDCPLL_CON1		(APMIXED_BASE + 0x518)
#define MSDCPLL_CON2		(APMIXED_BASE + 0x51c)
#define MSDCPLL_CON3		(APMIXED_BASE + 0x520)
#define TVDPLL1_CON0		(APMIXED_BASE + 0x524)
#define TVDPLL1_CON1		(APMIXED_BASE + 0x528)
#define TVDPLL1_CON2		(APMIXED_BASE + 0x52c)
#define TVDPLL1_CON3		(APMIXED_BASE + 0x530)
#define TVDPLL2_CON0		(APMIXED_BASE + 0x534)
#define TVDPLL2_CON1		(APMIXED_BASE + 0x538)
#define TVDPLL2_CON2		(APMIXED_BASE + 0x53c)
#define TVDPLL2_CON3		(APMIXED_BASE + 0x540)
#define MMPLL_CON0		(APMIXED_BASE + 0x544)
#define MMPLL_CON1		(APMIXED_BASE + 0x548)
#define MMPLL_CON2		(APMIXED_BASE + 0x54c)
#define MMPLL_CON3		(APMIXED_BASE + 0x550)
#define IMGPLL_CON0		(APMIXED_BASE + 0x554)
#define IMGPLL_CON1		(APMIXED_BASE + 0x558)
#define IMGPLL_CON2		(APMIXED_BASE + 0x55c)
#define IMGPLL_CON3		(APMIXED_BASE + 0x560)
#define ULPOSC1_CON0		(APMIXED_BASE + 0x600)
#define ULPOSC1_CON1		(APMIXED_BASE + 0x604)
#define ULPOSC1_CON2		(APMIXED_BASE + 0x608)

/* TOPCKGEN */
#define CLK_CFG_UPDATE		(TOPCKGEN_BASE + 0x4)
#define CLK_CFG_UPDATE1		(TOPCKGEN_BASE + 0x8)
#define CLK_CFG_UPDATE2		(TOPCKGEN_BASE + 0xc)
#define CLK_CFG_UPDATE3		(TOPCKGEN_BASE + 0x10)
#define CLK_CFG_0		(TOPCKGEN_BASE + 0x20)
#define CLK_CFG_0_SET		(TOPCKGEN_BASE + 0x24)
#define CLK_CFG_0_CLR		(TOPCKGEN_BASE + 0x28)
#define CLK_CFG_1		(TOPCKGEN_BASE + 0x2c)
#define CLK_CFG_1_SET		(TOPCKGEN_BASE + 0x30)
#define CLK_CFG_1_CLR		(TOPCKGEN_BASE + 0x34)
#define CLK_CFG_2		(TOPCKGEN_BASE + 0x38)
#define CLK_CFG_2_SET		(TOPCKGEN_BASE + 0x3c)
#define CLK_CFG_2_CLR		(TOPCKGEN_BASE + 0x40)
#define CLK_CFG_3		(TOPCKGEN_BASE + 0x44)
#define CLK_CFG_3_SET		(TOPCKGEN_BASE + 0x48)
#define CLK_CFG_3_CLR		(TOPCKGEN_BASE + 0x4c)
#define CLK_CFG_4		(TOPCKGEN_BASE + 0x50)
#define CLK_CFG_4_SET		(TOPCKGEN_BASE + 0x54)
#define CLK_CFG_4_CLR		(TOPCKGEN_BASE + 0x58)
#define CLK_CFG_5		(TOPCKGEN_BASE + 0x5c)
#define CLK_CFG_5_SET		(TOPCKGEN_BASE + 0x60)
#define CLK_CFG_5_CLR		(TOPCKGEN_BASE + 0x64)
#define CLK_CFG_6		(TOPCKGEN_BASE + 0x68)
#define CLK_CFG_6_SET		(TOPCKGEN_BASE + 0x6c)
#define CLK_CFG_6_CLR		(TOPCKGEN_BASE + 0x70)
#define CLK_CFG_7		(TOPCKGEN_BASE + 0x74)
#define CLK_CFG_7_SET		(TOPCKGEN_BASE + 0x78)
#define CLK_CFG_7_CLR		(TOPCKGEN_BASE + 0x7c)
#define CLK_CFG_8		(TOPCKGEN_BASE + 0x80)
#define CLK_CFG_8_SET		(TOPCKGEN_BASE + 0x84)
#define CLK_CFG_8_CLR		(TOPCKGEN_BASE + 0x88)
#define CLK_CFG_9		(TOPCKGEN_BASE + 0x8c)
#define CLK_CFG_9_SET		(TOPCKGEN_BASE + 0x90)
#define CLK_CFG_9_CLR		(TOPCKGEN_BASE + 0x94)
#define CLK_CFG_10		(TOPCKGEN_BASE + 0x98)
#define CLK_CFG_10_SET		(TOPCKGEN_BASE + 0x9c)
#define CLK_CFG_10_CLR		(TOPCKGEN_BASE + 0xa0)
#define CLK_CFG_11		(TOPCKGEN_BASE + 0xa4)
#define CLK_CFG_11_SET		(TOPCKGEN_BASE + 0xa8)
#define CLK_CFG_11_CLR		(TOPCKGEN_BASE + 0xac)
#define CLK_CFG_12		(TOPCKGEN_BASE + 0xb0)
#define CLK_CFG_12_SET		(TOPCKGEN_BASE + 0xb4)
#define CLK_CFG_12_CLR		(TOPCKGEN_BASE + 0xb8)
#define CLK_CFG_13		(TOPCKGEN_BASE + 0xbc)
#define CLK_CFG_13_SET		(TOPCKGEN_BASE + 0xc0)
#define CLK_CFG_13_CLR		(TOPCKGEN_BASE + 0xc4)
#define CLK_CFG_14		(TOPCKGEN_BASE + 0xc8)
#define CLK_CFG_14_SET		(TOPCKGEN_BASE + 0xcc)
#define CLK_CFG_14_CLR		(TOPCKGEN_BASE + 0xd0)
#define CLK_CFG_15		(TOPCKGEN_BASE + 0xd4)
#define CLK_CFG_15_SET		(TOPCKGEN_BASE + 0xd8)
#define CLK_CFG_15_CLR		(TOPCKGEN_BASE + 0xdc)
#define CLK_CFG_16		(TOPCKGEN_BASE + 0xe0)
#define CLK_CFG_16_SET		(TOPCKGEN_BASE + 0xe4)
#define CLK_CFG_16_CLR		(TOPCKGEN_BASE + 0xe8)
#define CLK_CFG_17		(TOPCKGEN_BASE + 0xec)
#define CLK_CFG_17_SET		(TOPCKGEN_BASE + 0xf0)
#define CLK_CFG_17_CLR		(TOPCKGEN_BASE + 0xf4)
#define CLK_CFG_18		(TOPCKGEN_BASE + 0xf8)
#define CLK_CFG_18_SET		(TOPCKGEN_BASE + 0xfc)
#define CLK_CFG_18_CLR		(TOPCKGEN_BASE + 0x100)
#define CLK_CFG_19		(TOPCKGEN_BASE + 0x104)
#define CLK_CFG_19_SET		(TOPCKGEN_BASE + 0x108)
#define CLK_CFG_19_CLR		(TOPCKGEN_BASE + 0x10c)
#define CLK_CFG_20		(TOPCKGEN_BASE + 0x110)
#define CLK_CFG_20_SET		(TOPCKGEN_BASE + 0x114)
#define CLK_CFG_20_CLR		(TOPCKGEN_BASE + 0x118)
#define CLK_CFG_21		(TOPCKGEN_BASE + 0x11c)
#define CLK_CFG_21_SET		(TOPCKGEN_BASE + 0x120)
#define CLK_CFG_21_CLR		(TOPCKGEN_BASE + 0x124)
#define CLK_CFG_22		(TOPCKGEN_BASE + 0x128)
#define CLK_CFG_22_SET		(TOPCKGEN_BASE + 0x12c)
#define CLK_CFG_22_CLR		(TOPCKGEN_BASE + 0x130)
#define CLK_CFG_23		(TOPCKGEN_BASE + 0x134)
#define CLK_CFG_23_SET		(TOPCKGEN_BASE + 0x138)
#define CLK_CFG_23_CLR		(TOPCKGEN_BASE + 0x13c)
#define CLK_CFG_24		(TOPCKGEN_BASE + 0x140)
#define CLK_CFG_24_SET		(TOPCKGEN_BASE + 0x144)
#define CLK_CFG_24_CLR		(TOPCKGEN_BASE + 0x148)
#define CLK_CFG_25		(TOPCKGEN_BASE + 0x14c)
#define CLK_CFG_25_SET		(TOPCKGEN_BASE + 0x150)
#define CLK_CFG_25_CLR		(TOPCKGEN_BASE + 0x154)
#define CLK_CFG_26		(TOPCKGEN_BASE + 0x158)
#define CLK_CFG_26_SET		(TOPCKGEN_BASE + 0x15c)
#define CLK_CFG_26_CLR		(TOPCKGEN_BASE + 0x160)
#define CLK_EXTCK_REG		(TOPCKGEN_BASE + 0x204)
#define CLK_DBG_CFG		(TOPCKGEN_BASE + 0x20c)
#define CLK26CALI_0		(TOPCKGEN_BASE + 0x218)
#define CLK26CALI_1		(TOPCKGEN_BASE + 0x21c)
#define CLK_MISC_CFG_0		(TOPCKGEN_BASE + 0x22c)
#define CLK_MISC_CFG_1		(TOPCKGEN_BASE + 0x238)
#define CLK_MISC_CFG_2		(TOPCKGEN_BASE + 0x244)
#define CLK_MISC_CFG_3		(TOPCKGEN_BASE + 0x250)
#define CLK_MISC_CFG_6		(TOPCKGEN_BASE + 0x25c)
#define CLK_SCP_CFG_0		(TOPCKGEN_BASE + 0x264)

/* MCUSYS */
#define CPU_PLLDIV_CFG0		(MCUSYS_CFGREG_BASE + 0xA2A0)
#define CPU_PLLDIV_CFG1		(MCUSYS_CFGREG_BASE + 0xA2A4)
#define BUS_PLLDIV_CFG		(MCUSYS_CFGREG_BASE + 0xA2E0)

/* INFRACFG_AO */
#define INFRACFG_AO_MODULE_SW_CG_0_SET	(INFRACFG_AO_BASE + 0x80)
#define INFRACFG_AO_MODULE_SW_CG_0_CLR	(INFRACFG_AO_BASE + 0x84)
#define INFRACFG_AO_MODULE_SW_CG_1_SET	(INFRACFG_AO_BASE + 0x88)
#define INFRACFG_AO_MODULE_SW_CG_1_CLR	(INFRACFG_AO_BASE + 0x8c)
#define INFRACFG_AO_MODULE_SW_CG_2_SET	(INFRACFG_AO_BASE + 0xa4)
#define INFRACFG_AO_MODULE_SW_CG_2_CLR	(INFRACFG_AO_BASE + 0xa8)
#define INFRACFG_AO_MODULE_SW_CG_3_SET	(INFRACFG_AO_BASE + 0xc0)
#define INFRACFG_AO_MODULE_SW_CG_3_CLR	(INFRACFG_AO_BASE + 0xc4)
#define INFRACFG_AO_MODULE_SW_CG_4_SET	(INFRACFG_AO_BASE + 0xe0)
#define INFRACFG_AO_MODULE_SW_CG_4_CLR	(INFRACFG_AO_BASE + 0xe4)

/* SUBSYS */
#define AUDIO_SRC_MEM_ASRC_TOP_CON1		(AUDIO_SRC_BASE + 0x1004)
#define AUDIO_AUDIO_TOP_0			(AUDIO_BASE + 0x0)
#define AUDIO_AUDIO_TOP_4			(AUDIO_BASE + 0x10)
#define AUDIO_AUDIO_TOP_5			(AUDIO_BASE + 0x14)
#define AUDIO_AUDIO_TOP_6			(AUDIO_BASE + 0x18)
#define AUDIO_AUDIO_TOP_1			(AUDIO_BASE + 0x4)
#define AUDIO_AUDIO_TOP_2			(AUDIO_BASE + 0x8)
#define AUDIO_AUDIO_TOP_3			(AUDIO_BASE + 0xc)
#define PERICFG_AO_PERI_MODULE_SW_CG_0_CLR	(PERICFG_AO_BASE + 0x14)
#define IMP_IIC_WRAP_C_AP_CLOCK_CG_CEN_CLR	(IMP_IIC_WRAP_C_BASE + 0xe04)
#define IMP_IIC_WRAP_W_AP_CLOCK_CG_WST_CLR	(IMP_IIC_WRAP_W_BASE + 0xe04)
#define IMP_IIC_WRAP_EN_AP_CLOCK_CG_EST_NOR_CLR	(IMP_IIC_WRAP_EN_BASE + 0xe04)
#define MFGCFG_MFG_CG_CLR			(MFGCFG_BASE + 0x8)
#define VPP0_REG_VPPSYS0_CG0_CLR		(VPP0_REG_BASE + 0x28)
#define VPP0_REG_VPPSYS0_CG1_CLR		(VPP0_REG_BASE + 0x34)
#define VPP0_REG_VPPSYS0_CG2_CLR		(VPP0_REG_BASE + 0x40)
#define WPESYS_TOP_REG_WPESYS_RG_000		(WPESYS_TOP_REG_BASE + 0x0)
#define WPE_VPP0_CTL_WPE_DCM_DIS		(WPE_VPP0_BASE + 0x58)
#define WPE_VPP0_CTL_DMA_DCM_DIS		(WPE_VPP0_BASE + 0x5c)
#define VPPSYS1_CONFIG_VPPSYS1_CG_0_CLR		(VPPSYS1_CONFIG_BASE + 0x108)
#define VPPSYS1_CONFIG_VPPSYS1_CG_1_CLR		(VPPSYS1_CONFIG_BASE + 0x118)
#define IMGSYS_MAIN_IMG_MAIN_CG_CLR		(IMGSYS_MAIN_BASE + 0x8)
#define IMGSYS1_DIP_TOP_MACRO_CG_CLR		(IMGSYS1_DIP_TOP_BASE + 0x8)
#define IMGSYS1_DIP_NR_MACRO_CG_CLR		(IMGSYS1_DIP_NR_BASE + 0x8)
#define IMGSYS_WPE1_MACRO_CG_CLR		(IMGSYS_WPE1_BASE + 0x8)
#define IPESYS_MACRO_CG_CLR			(IPESYS_BASE + 0x8)
#define IMGSYS_WPE2_MACRO_CG_CLR		(IMGSYS_WPE2_BASE + 0x8)
#define IMGSYS_WPE3_MACRO_CG_CLR		(IMGSYS_WPE3_BASE + 0x8)
#define CAMSYS_MAIN_CAM_MAIN_CG_CLR		(CAMSYS_MAIN_BASE + 0x8)
#define CAMSYS_RAWA_CAMSYS_CG_CLR		(CAMSYS_RAWA_BASE + 0x8)
#define CAMSYS_YUVA_CAMSYS_CG_CLR		(CAMSYS_YUVA_BASE + 0x8)
#define CAMSYS_RAWB_CAMSYS_CG_CLR		(CAMSYS_RAWB_BASE + 0x8)
#define CAMSYS_YUVB_CAMSYS_CG_CLR		(CAMSYS_YUVB_BASE + 0x8)
#define CCU_MAIN_CCUSYS_CG_CLR			(CCU_MAIN_BASE + 0x8)
#define VDEC_SOC_GCON_VDEC_CKEN			(VDEC_SOC_GCON_BASE + 0x0)
#define VDEC_SOC_GCON_LAT_CKEN			(VDEC_SOC_GCON_BASE + 0x200)
#define VDEC_SOC_GCON_LARB_CKEN_CON		(VDEC_SOC_GCON_BASE + 0x8)
#define VDEC_GCON_VDEC_CKEN			(VDEC_GCON_BASE + 0x0)
#define VDEC_GCON_LAT_CKEN			(VDEC_GCON_BASE + 0x200)
#define VDEC_GCON_LARB_CKEN_CON			(VDEC_GCON_BASE + 0x8)
#define VENC_GCON_VENCSYS_CG_SET		(VENC_GCON_BASE + 0x4)
#define VDOSYS0_CONFIG_GLOBAL0_CG_0_CLR		(VDOSYS0_CONFIG_BASE + 0x108)
#define VDOSYS0_CONFIG_GLOBAL0_CG_1_CLR		(VDOSYS0_CONFIG_BASE + 0x118)
#define VDOSYS0_CONFIG_GLOBAL0_CG_2_CLR		(VDOSYS0_CONFIG_BASE + 0x128)
#define VDOSYS1_CONFIG_VDOSYS1_CG_0_CLR		(VDOSYS1_CONFIG_BASE + 0x108)
#define VDOSYS1_CONFIG_VDOSYS1_CG_1_CLR		(VDOSYS1_CONFIG_BASE + 0x118)
#define VDOSYS1_CONFIG_VDOSYS1_CG_2_CLR		(VDOSYS1_CONFIG_BASE + 0x128)
#define VDOSYS1_CONFIG_VDOSYS1_CG_3_CLR		(VDOSYS1_CONFIG_BASE + 0x138)
#define VDOSYS1_CONFIG_VDOSYS1_CG_4_CLR		(VDOSYS1_CONFIG_BASE + 0x148)

/* INFRA_AO_BCRM */
#define VDNR_DCM_TOP_INFRA_CTRL0		(INFRA_AO_BCRM_BASE + 0x48)

/* SPM */
#define EXT_BUCK_ISO				(SPM_BASE + 0x3EC)
#define AP_MDSRC_REQ				(SPM_BASE + 0x43C)

/*  CPU Freq Boost*/
enum cpu_opp {
    CPU_OPP0 = 0,
    CPU_OPP1,
    CPU_OPP2,
    CPU_OPP3,
    CPU_OPP4,
    CPU_OPP5,
    CPU_OPP_NUM,
};

/*
 * EXTERN FUNCTIONS
 */
extern unsigned int mt_get_abist_freq(unsigned int ID);
extern unsigned int mt_get_cpu_freq(void);
extern void set_armpll_ll_rate(enum cpu_opp opp);
extern void mt_set_topck_default(void);
extern void mt_pll_init(void);
#endif
