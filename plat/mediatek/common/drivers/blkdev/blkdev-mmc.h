/* SPDX-License-Identifier: BSD-3-Clause */
/*
 * Copyright (c) 2024 MediaTek Inc.
 */

#ifndef BLKDEV_MMC_H
#define BLKDEV_MMC_H

#include "blkdev.h"

unsigned int mmc_register_blkdev(void);

#endif
