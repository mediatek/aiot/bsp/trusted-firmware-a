/* SPDX-License-Identifier: BSD-3-Clause */
/*
 * Copyright (c) 2024 MediaTek Inc.
 */

#pragma once

#include "blkdev.h"

unsigned int nor_register_blkdev(void);
