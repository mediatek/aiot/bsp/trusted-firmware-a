/*
 * Copyright (c) 2016-2022, ARM Limited and Contributors. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include <arch_helpers.h>
#include <common/bl_common.h>
#include <common/debug.h>
#include <drivers/arm/cci.h>
#include <drivers/console.h>
#include <drivers/mmc.h>
#include <drivers/partition/partition.h>
#include <lib/mmio.h>
#include <lib/smccc.h>
#include <lib/xlat_tables/xlat_tables.h>
#include <plat/common/platform.h>
#include <services/arm_arch_svc.h>

#include <mtk_plat_common.h>
#include <mtk_sip_svc.h>
#include <plat_private.h>

void clean_top_32b_of_param(uint32_t smc_fid,
				u_register_t *px1,
				u_register_t *px2,
				u_register_t *px3,
				u_register_t *px4)
{
	/* if parameters from SMC32. Clean top 32 bits */
	if (GET_SMC_CC(smc_fid) == SMC_64) {
		*px1 = *px1 & SMC32_PARAM_MASK;
		*px2 = *px2 & SMC32_PARAM_MASK;
		*px3 = *px3 & SMC32_PARAM_MASK;
		*px4 = *px4 & SMC32_PARAM_MASK;
	}
}

/*****************************************************************************
 * plat_is_smccc_feature_available() - This function checks whether SMCCC
 *                                     feature is availabile for platform.
 * @fid: SMCCC function id
 *
 * Return SMC_OK if SMCCC feature is available and SMC_ARCH_CALL_NOT_SUPPORTED
 * otherwise.
 *****************************************************************************/
int32_t plat_is_smccc_feature_available(u_register_t fid)
{
	switch (fid) {
	case SMCCC_ARCH_SOC_ID:
		return SMC_ARCH_CALL_SUCCESS;
	default:
		return SMC_ARCH_CALL_NOT_SUPPORTED;
	}
}

int32_t plat_get_soc_version(void)
{
	uint32_t manfid = SOC_ID_SET_JEP_106(JEDEC_MTK_BKID, JEDEC_MTK_MFID);

	return (int32_t)(manfid | (SOC_CHIP_ID & SOC_ID_IMPL_DEF_MASK));
}

int32_t plat_get_soc_revision(void)
{
	return 0;
}

#if defined(STORAGE_UFS) || defined(STORAGE_NOR)
char *get_boot_partition_name(void)
{
	return "bootloaders";
}
#else
char *get_boot_partition_name(void)
{
	unsigned char current_boot_part = mmc_current_boot_part();

	switch (current_boot_part) {
	case 1U:
		INFO("Current boot part is 1 so fip.bin is bootloaders\n");
		return "bootloaders";
	case 2U:
		INFO("Current boot part is 2 so fip.bin is bootloaders2\n");
		return "bootloaders2";
	default:
		ERROR("Got unexpected value for active boot partition, %u\n", current_boot_part);
		return "";
	}
}
#endif

uint64_t get_part_addr(const char *name)
{
#ifdef STORAGE_NOR
	if (strncmp(name, "dramk", 5) == 0)
		return 0x1400000;
	return 0;
#else
	const partition_entry_t *entry;

	entry = get_partition_entry(name);
	if (entry == NULL) {
		NOTICE("Could NOT find the %s partition!\n", name);
		return 0;
	}
	return entry->start;
#endif
}
