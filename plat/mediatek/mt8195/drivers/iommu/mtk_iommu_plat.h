/*
 * Copyright (c) 2022, MediaTek Inc. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#ifndef IOMMU_PLAT_H
#define IOMMU_PLAT_H

/* debug log */
#define IMU_LOG_DBG_EN			0

/* secure iommu */
#define ATF_MTK_IOMMU_FAULT_REPORT_SUPPORT
#define ATF_MTK_IOMMU_RUNTIME_RS_SUPPORT

/* iommu secure pagetable mapping */
#define ATF_MTK_IOMMU_PTBL_MAPPING_SUPPORT

#endif /* IOMMU_PLAT_H */
